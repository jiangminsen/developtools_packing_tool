/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ohos;

public class Constants {
    public static final String SLASH = "/";
    public static final String COMMA = ",";
    public static final String RAW_FILE_PATH = "resources/rawfile";
    public static final String RES_FILE_PATH = "resources/resfile";
    public static final String JSON_SUFFIX = ".json";
    public static final String HAP_SUFFIX = ".hap";
    public static final String HSP_SUFFIX = ".hsp";
    public static final String APP_SUFFIX = ".app";
    public static final String PNG_SUFFIX = ".app";
    public static final String APP = "app";
    public static final String BUNDLE_TYPE = "bundleType";
    public static final String BUNDLE_TYPE_SHARED = "shared";
    public static final String BUNDLE_TYPE_APP_SERVICE = "appService";
    public static final String SUMMARY = "summary";
    public static final String PACKAGES = "packages";
    public static final String MODULE = "module";
    public static final String BUILD_HASH = "buildHash";
    public static final String MODULE_NAME = "name";
    public static final String MODULE_TYPE = "type";
    public static final String TYPE_SHARED = "shared";
    public static final String TYPE_ENTRY = "entry";
    public static final String TYPE_FEATURE = "feature";
    public static final String GENERATE_BUILD_HASH = "generateBuildHash";
    public static final String COMPRESS_NATIVE_LIBS = "compressNativeLibs";
    public static final String FILE_PACK_INFO = "pack.info";
    public static final String FILE_MODULE_JSON = "module.json";
    public static final String FILE_CONFIG_JSON = "config.json";
    public static final String FILE_PACK_RES = "pack.res";
    public static final String NULL_DIR = "";
    public static final String LIBS_DIR = "libs";
    public static final String SHA_256 = "SHA-256";
    public static final String TMP = "tmp";
    public static final String TRUE = "true";
    public static final String FALSE = "false";
}
